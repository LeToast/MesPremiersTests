class BookRepository {

    /**
     * @param db
     */
    constructor(db) {
        this.db = db;
    }

    save (book) {
        this.db
            .get('books')
            .push(book)
            .write();
    }

    /**
     * Nombre total de livre
     */
    getTotalCount() {
        return this.db.get('books').size().value()
    }

    /**
     * Somme du prix de tous les livre
     */
    getTotalPrice() {
        return this.db.get('books').sumBy("price").value();
    }

    /**
     * Retourne un livre
     */
    getBookByName(bookName) {
        return this.db.get('books').find({'name':bookName}).value();
    }

    /**
     * Nombre de livre ajouté par mois
     *
     *  [
     *      {
     *          year: 2017,
     *          month, 2,
     *          count, 129,
     *          count_cumulative: 129
     *      },
     *      {
     *          year: 2017,
     *          month, 3,
     *          count, 200,
     *          count_cumulative: 329
     *      },
     *      ....
     *  ]
     */
    getCountBookAddedByMonth() {
        const month = item => new Date(item.added_at).getMonth()+1;
        return this.db.get('books')
            .countBy(month)
            .value();
    }

}


module.exports = BookRepository;