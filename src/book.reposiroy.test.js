const BookRepository = require('./book.repository');

describe('Book repository Save', function () {

    test('Save a book', () => {

        const dbMock = {
            get : jest.fn(),
            push : jest.fn(),
            write : jest.fn()
        };
        dbMock.get.mockReturnValue(dbMock);
        dbMock.push.mockReturnValue(dbMock);

        const repository = new BookRepository(dbMock);
        repository.save({id: 1, name: "Unit test"});

        expect(dbMock.write.mock.calls.length).toBe(1);
    });
});
describe("book Respository get total items", function (){
    test("get total books", () => {
        let dbMock = {
            get : jest.fn(),
            size : jest.fn(),
            value : jest.fn()
        };
        dbMock.get.mockReturnValue(dbMock)
        dbMock.size.mockReturnValue(dbMock)
        dbMock.value.mockReturnValue(9000)
        const repository = new BookRepository(dbMock);
        expect(repository.getTotalCount()).toBe(9000);
        expect(dbMock.size.mock.calls.length).toBe(1);

    });
})
describe("book Respository get total price of items", function (){

    test("get total price", () => {
        let dbMock = {
            get : jest.fn(),
            sumBy : jest.fn(),
            value : jest.fn()
        };
        dbMock.get.mockReturnValue(dbMock)
        dbMock.sumBy.mockReturnValue(dbMock)
        dbMock.value.mockReturnValue(200)
        const repository = new BookRepository(dbMock);
        expect(repository.getTotalPrice()).toBe(200);
        expect(dbMock.sumBy.mock.calls.length).toBe(1);

    });

})
describe("book Respository get book by name", function (){

    test("get name of book", () => {
        let dbMock = {
            get : jest.fn(),
            find : jest.fn(),
            value : jest.fn()
        };
        dbMock.get.mockReturnValue(dbMock)
        dbMock.find.mockReturnValue(dbMock)
        dbMock.value.mockReturnValue(200)
        const repository = new BookRepository(dbMock);
        expect(repository.getBookByName("toto")).toBe(200);
        expect(dbMock.find.mock.calls.length).toBe(1);

    });

})
describe("book Respository get book added by month", function (){

    test("get number of books added by month", () => {
        let dbMock = {
            get : jest.fn(),
            countBy : jest.fn(),
            value : jest.fn()
        };
        dbMock.get.mockReturnValue(dbMock)
        dbMock.countBy.mockReturnValue(dbMock)
        dbMock.value.mockReturnValue(200)
        const repository = new BookRepository(dbMock);
        expect(repository.getCountBookAddedByMonth("toto")).toBe(200);
        expect(dbMock.countBy.mock.calls.length).toBe(1);

    });

})